import './App.css';

import {w3cwebsocket as W3CWebSocket} from "websocket";
import React from "react";
import {toast, ToastContainer} from "react-toastify";
import Button from 'react-bootstrap/Button'
import 'react-toastify/dist/ReactToastify.css';

const api_token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjoiYkwrNE5IVDlHdWx2QWU4clhVQmVKNFJaNG02UHd0bmUxRDBQcW9sdXVqa21VOG9vR1lDTkdSalYzUW5kVUxQRVgyVTdLN2dnZGIzVmU4ZlFhclJnT3NYSlQrSUhLL05kUWlUUVRRR0U1WjVCbkVUekU2SFNOa0NFQ3NDVUhoWjE4RE1yQ1hzVTFXK3hjUXI1VzlIM3FkcGxWQWh2TEpTZGlKNGxMZm41NWN3K3g0SkRSbERWRUloTnRYbWdVNGRBYnY4WWJCaW9nazFrTWltU0FHWmluTFF5TXFVRnpHU09zbURXaGgxZEM3ZVQrUVR6UExqYmRaVndUUFE3REpadHN5Y2lzS1ZGRXQveE8wQWk1Vm01Yy9MeGQ1emRFOFYzMzFHb1ZsT0l1YzFsa08yU0JjTTEycGQ1b3NUY0JHVzJNdTF3cmRqalpCMFJXRDl5cmV6Z0ZEUm9FVWw2U3dNL014MmhsT2wyZGlYdWsrZmxUdVNLUE1Xd1ZOa3dNd0pVUVJ5VU1JU0l6bHl4ZERvaXFVa3FBM1habHpBVC9FVC9TMGVydzhySjBsOVErbmFISit6clByb1hzTWRwTHJoanZMcXVuV1NjS2NxYUxyRHNmSEt1SnJmTktWVWluQVVSdGdDYnNOMGNSWkJRcGx1K1dvMWp4VnFEM3Yrb0pNbkova0Q1RDl4eVdjc1RRbDVGSkNpYWNBPT0iLCJpYXQiOjE2NDM3MjQwMDAsImV4cCI6MTY0MzgxMDQwMH0.zME4A-Q2kWCKSkvL2OzRgFLz_UjUu2fonj0dckOWwlk"

const client = new W3CWebSocket("wss://ws-api.enigma-x.app/?token=" + api_token);


class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            amount: 0,
            buy_price: 35000,
            sell_price: 36000
        }
    }


    componentDidMount() {
        client.onopen = () => {
            console.log('WebSocket Client Connected');
            const request = {
                "type": "subscription",
                "id": "bf5d15d0-415f-11ec-b255-ad01e0712738",
                "data": {
                    "products": ["BTC-USD"],
                    "quantity": "1.00000000",
                    "level": true,
                    "high": true,
                    "low": true,
                    "change_daily": true
                }
            }
            client.send(JSON.stringify(request))

        };

        client.onmessage = (message) => {
            const response = JSON.parse(message.data)
            if (response.type === "execution") {
                if (response.code === 200) {
                    this.notify("Success!")
                } else if (response.code === 500) {
                    this.notify("Error : " + response.content['message'])
                }
            }
            if (response.type === "subscription" && response.code === 200) {
                const buy_price = response.content["BTC-USD"].price.ask.price
                const sell_price = response.content["BTC-USD"].price.bid.price
                this.setState({
                    buy_price: buy_price,
                    sell_price: sell_price
                })

            }
        };
    }

    buy_sell = (type) => {
        let price;
        if (type === "BUY") {
            price = this.state.buy_price
        } else {
            price = this.state.sell_price
        }
        let request = {
            "type": "execution",
            "id": "dc7d7e2c-2155-475b-b31f-76dbced95c6b",
            "data": {
                "product": "BTC-USD",
                "side": type,
                "quantity": this.state.amount,
                "price": price,
                "type": "LIMIT",
                "time_in_force": "FOK"
            }
        }
        client.send(JSON.stringify(request))
    }

    onValueChange = (event) => {
        this.setState({
            amount: event.target.value
        })
    }
    notify = (message) => toast(message);

    render() {
        return (
            <div className="App container" style={{width: '50%'}}>
                <h1>BTC - USDT</h1>
                <div>
                    <input value={this.state.amount}
                           onChange={this.onValueChange}/>
                    <div className="d-flex flex-column">
                        <div className="p-2">
                            <div className="d-flex col align-items-center justify-content-center">
                                <Button variant="success"
                                        onClick={() => {
                                            this.buy_sell("BUY")
                                        }}>Buy</Button>
                                <Button className="mx-2" variant="danger"
                                        onClick={() => {
                                            this.buy_sell("SELL")
                                        }}>Sell</Button>
                            </div>
                        </div>
                        <div className="p-2">
                            <div className="d-flex col align-items-center justify-content-center">
                                <div>
                                    {this.state.buy_price}
                                </div>
                                <div className="mx-2">
                                    {this.state.sell_price}

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <ToastContainer
                    position="top-right"
                    autoClose={5000}
                    hideProgressBar={false}
                    newestOnTop={false}
                    closeOnClick
                    rtl={false}
                    pauseOnFocusLoss
                    draggable
                    pauseOnHover
                />
            </div>
        );
    }

}

export default App;
